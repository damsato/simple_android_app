package com.example.sample14;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class MainActivityTest {
    MainActivity activity;

    @Before
    public void setUp(){
        activity = new MainActivity();
    }

    @Test
    public void TestAddition() {
        String x = "1.0";
        String y = "2.0";
        assertThat(activity.makeResult(x, y, R.id.radioButtonAddition), is("1.0 + 2.0 = 3.0"));
    }

    @Test
    public void TestSubtraction() {
        String x = "1.0";
        String y = "2.0";
        assertThat(activity.makeResult(x, y, R.id.radioButtonSubtraction), is("1.0 - 2.0 = -1.0"));
    }

}